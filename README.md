
#### react native learing

#### 目录说明

#### 依赖相关
+ navigation
    - @react-navigation/native
    - @react-navigation/stack
    - @react-navigation/bottom-tabs
    - react-native-reanimated
    - react-native-gesture-handler  // 用于手势切换页面
    - react-native-screens // 用于原生层释放未展示的页面，改善 app 内存使用
    - react-native-safe-area-context // 用于保证页面显示在安全区域（主要针对刘海屏）
    - @react-native-community/masked-view // 用在头部导航栏中返回按钮的颜色设置

+ 图标
    - react-native-vector-icons
    - AntDesign图片配置(添加下面代码，别问为什么，cv仔不懂)
        + ios(ios/rnapp/info.plist)
            ```
            <key>UIAppFonts</key>
                <array>
                    <string>AntDesign.ttf</string>
                    <string>Entypo.ttf</string>
                    <string>EvilIcons.ttf</string>
                    <string>Feather.ttf</string>
                    <string>FontAwesome.ttf</string>
                    <string>FontAwesome5_Brands.ttf</string>
                    <string>FontAwesome5_Regular.ttf</string>
                    <string>FontAwesome5_Solid.ttf</string>
                    <string>Foundation.ttf</string>
                    <string>Ionicons.ttf</string>
                    <string>MaterialIcons.ttf</string>
                    <string>MaterialCommunityIcons.ttf</string>
                    <string>SimpleLineIcons.ttf</string>
                    <string>Octicons.ttf</string>
                    <string>Zocial.ttf</string>
                    <string>Fontisto.ttf</string>
                </array>
            ```

        + android(android/app/build.gradle)
        ```
        project.ext.vectoricons = [
                iconFontNames: ['AntDesign.ttf',
                                'Entypo.ttf',
                                'EvilIcons.ttf',
                                'Feather.ttf',
                                'FontAwesome.ttf',
                                'FontAwesome5_Brands.ttf',
                                'FontAwesome5_Regular.ttf',
                                'FontAwesome5_Solid.ttf',
                                'Foundation.ttf',
                                'Ionicons.ttf',
                                'MaterialIcons.ttf',
                                'MaterialCommunityIcons.ttf',
                                'SimpleLineIcons.ttf',
                                'Octicons.ttf',
                                'Zocial.ttf',
                                'Fontisto.ttf'] // Name of the font files you want to copy
        ]
        
        apply from: "../../node_modules/react-native-vector-icons/fonts.gradle"
        ```

+ 自定义图标还有看(https://www.jianshu.com/p/a365f214c94d)

+ 配置目录引用(如@app/)
  ```
  // 在app目录下创建package.json
  {
      "name": "@app"
  }
  ```

+ 配置全局变量
`在app的的入口js下引用定义的变量，如本项目在constants目录下的global.js定义了全局方法和变量，在根目录下的入口文件index.js直接引用import '@app/constants/global'，之后就可以在项目中各处使用`

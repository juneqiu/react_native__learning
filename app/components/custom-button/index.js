import React from 'react'
import { TouchableOpacity } from 'react-native'

export default ({ children, activeOpacity = 0.9, style = {}, onClick = () => {} }) => {
    return (
        <TouchableOpacity 
            activeOpacity = { activeOpacity } 
            style = { style } 
            onPress= { onClick }
        >
            { children || null }
        </TouchableOpacity>
    )
}
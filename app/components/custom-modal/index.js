import React, { useState } from 'react'
import { Modal, StyleSheet, Pressable, Alert } from 'react-native'
import { View, Text } from '@app/components/custom-theme' 
/**
 * @ 不要在content层留View不然点击会穿透
 * @param { String } position content的位置
 */ 
const CustomModal = props => {
    const [modalVisible, setModalVisible] = useState(true)
    
    console.log(props)
    return (
        <Modal
            style={ styles.madal_el }
            animationType='fade' // slide:从底部滑出 fade:淡入淡出 node:没有动画，直接蹦出来 
            transparent={ true } // 背景是否透明
            visible={ modalVisible }
            onRequestClose={() => { // 回调会在用户按下 Android 设备上的后退按键或是 Apple TV 上的菜单键时触发 
                Alert.alert("Modal has been closed.");
            }}
        >
            <Pressable 
                style={[ styles.pressable_el, styles[props.position] ]}
                onPress= {() => {
                    Alert.alert('点击在mask')
                }}
            >
                { () => props._renderContent() }
            </Pressable>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modal_el: {
        flex: 1
    },
    pressable_el: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    start: {
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    end: {
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
})

export default CustomModal

import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 * 获取本地存储数据
 * @param { String } key 
 */
const getStorage = async key => {
    try {
        if(Object.prototype.toString.call(key) !== '[object String]') return null;
        const value = await AsyncStorage.getItem(key) || null;
        return value ? JSON.parse(value) : null;
    } catch (error) {
        console.log(error)
        return null;
    }
}

/**
 * 设置本地存储
 * @param { String } key 
 * @param { any } value 
 */
const setStorage = async (key, value) => {
    try {
        if(Object.prototype.toString.call(key) !== '[object String]' || !value) return null;
        return await AsyncStorage.setItem(key, typeof value === 'string' ? value : JSON.stringify(value))
    } catch (error) {
        return null;
    }
}

/**
 * 更新对应key的数据
 * @param { String } key
 * @param { any } value
 */
const updateStorage = async (key, value) => {
    try {
        console.log(key, value)
        if(Object.prototype.toString.call(key) !== '[object String]' || !value) return null;
        await AsyncStorage.setItem(key, typeof value === 'string' ? value : JSON.stringify(value));
    } catch (error) {
        console.log(error)
    }
}

/**
 * 删除对应key值的数据
 */
const removeStorage = async key => {
    if(Object.prototype.toString.call(key) !== '[object String]') return;
    return AsyncStorage.removeItem(key);
}

/**
 * 清楚缓存
 */
const clearStorage = async () => {
    try {
        AsyncStorage.clear();
    } catch (error) {
        console.log(error)
    }
}

export {
    getStorage,
    setStorage,
    updateStorage,
    removeStorage,
    clearStorage
}

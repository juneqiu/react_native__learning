import { Platform } from 'react-native'

/**
 * 判断是否IOS平台
 */
const isIOS = Platform.OS === 'ios'

/**
 * 判断是否Android平台
 */
const isAndroid = Platform.OS === 'android'

/**
 * 判断是否iPhoneX  X和XS  375*812
 */
const isIPhoneX = isIOS && window.width === 375 && window.height === 812

/**
 * 判断是否iPhoneXR  XS_Max和XR 414*896
 */
const isIPhoneXR = isIOS && window.width === 414 && window.height === 896

/**
 * 判断是否Android5.x平台
 */
const isAndroid5 = isAndroid && Platform.Version > 20 && Platform.Version < 23

/**
 * 判断是否Android5.0以上平台
 */
const overAndroid5 = isAndroid && Platform.Version > 19

/**
 * StatusBar高度
 */
const StatusBarHeight = isIOS ? ((isIPhoneX || isIPhoneXR) ? 44 : 20) : (overAndroid5 ? StatusBar.currentHeight : 0)

/**
 * 导航栏高度
 */
const NavBarHeight = isIOS ? 44 : px2dp(88)


export {
    isIOS,
    isAndroid,
    isIPhoneX,
    isIPhoneXR,
    isAndroid5,
    overAndroid5,
    
    StatusBarHeight,
    NavBarHeight,
}
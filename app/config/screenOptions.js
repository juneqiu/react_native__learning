import React from 'react'
import { StyleSheet, Animated, useColorScheme } from 'react-native'
import { TransitionPresets } from '@react-navigation/stack'
import { StatusBarHeight, NavBarHeight } from '@app/config/appInfo'
import Colors from '@app/constants/colors'

export default () => {
    const theme = useColorScheme()
    return {
        headerStyle: styles.header, // 自定义标题栏样式
        headerTintColor: '#333', // 返回按钮和标题都使用这个属性作为它们的颜色
        headerTitleStyle: [styles.headerTitle, {color: Colors[theme]['text']}], // 自定义标题文字样式
        headerBackTitleVisible: false, // 是否显示返回文字，Android 默认 false，iOS 默认 true
        headerTitleAlign: 'center', // 标题对齐方式，支持 left (Android 默认) / center (iOS 默认)
        cardStyle: [styles.card, { backgroundColor: theme === 'dark' ? '#f34250' : '#f3f3f3' }], // 页面 Card 的样式
        gestureEnabled: true, // 是否支持手势返回，iOS默认开启（不开启的话只能在页面上自定义返回按钮了），Android 默认是关闭的（Android 除了返回按钮，还有物理/虚拟返回键）
        transitionSpec: { timing: Animated.timing }, // 动效 config
        ...TransitionPresets.SlideFromRightIOS
    }
}



const styles = StyleSheet.create({
    header: {
        shadowOpacity: 0 // remove shadow on iOS
    },
    headerTitle: {
        fontSize: px2dp(32),
        alignSelf: 'center',
        textAlign: 'center'
    },
    card: {
        flex: 1
    }
})

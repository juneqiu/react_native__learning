import React, { useState, useEffect, useCallback } from 'react'
import { Image, Button , StyleSheet, Picker} from 'react-native'
import { View, Text } from '@app/components/custom-theme'
import styles from './styles'

import { getStorage,
    setStorage,
    updateStorage,
    removeStorage,
    clearStorage } from '@app/common/storage'

const HomeScreen = ()=> {
    const [token, setToken ] = useState('1')

    return (
        <>
            <Image
                style={ styles.top_img } 
                source={ require('@app/static/images/bg.png') }
            />
            <View>
                <Text>June</Text>
                <Text>男</Text>
            </View>

            <Button title='获取' onPress={() => get}></Button>
            <Button title='设置' onPress={() => set}></Button>
        </>
    );
};

export default HomeScreen

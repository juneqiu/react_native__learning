/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text } from '../components/custom-theme'

const Test2Screen = ()=> {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>测试2</Text>
        </View>
    );
};

export default Test2Screen;

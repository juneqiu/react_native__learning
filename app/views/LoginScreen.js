import React, { useState } from 'react';
import { Button, Alert } from 'react-native'
import { View, Text } from '../components/custom-theme'

const LoginScreen = ({ navigation }) => {
    

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: '#000', fontSize: px2(100)}}>登录测试页</Text>
            <Button 
                title='点我跳转'
                color="#841584"
                onPress={ () => {
                    navigation.navigate('TabNav')
                }} 
            />
            <View>
                <Text>sss</Text>
            </View>
        </View>
    )
}

export default LoginScreen

/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text } from '../components/custom-theme'

const Test1Screen = ()=> {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>测试页面1</Text>
        </View>
    );
};

export default Test1Screen;

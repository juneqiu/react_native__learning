import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    user_info__wrap: {
        width: px2(750),
        height: px2(240),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: px2(20)
    },
    avatar: {
        width: px2(160),
        height: px2(160),
        borderRadius: px2(160),
        marginRight: px2(20)
    },
    user_info: {
        flex: 1,
        justifyContent: 'space-around',
        height: px2(160)
    },
    nickname: {
        fontSize: px2(32),
        fontWeight: 'bold'
    },
    sex: {
        fontSize: px2(32),
        fontWeight: 'bold'
    }
})
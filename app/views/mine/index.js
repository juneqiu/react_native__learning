/* eslint-disable prettier/prettier */
import React from 'react';
import { Image } from 'react-native';
import { View, Text } from '@app/components/custom-theme'
import Button from '@app/components/custom-button'
import styles from './styles'
import colors from '../../constants/colors';

const MineScreen = ({navigation})=> {
   
    return (
        <>
            <View style={ styles.user_info__wrap }>
                <Image
                    style={ styles.avatar } 
                    source={ require('@app/static/images/bg.png') }
                /> 
                <View style={ styles.user_info }>
                    <Text style={ styles.nickname }>June</Text>
                    <Text style={ styles.sex }>男</Text>
                </View>
            </View>
            <Text>测试3</Text>
            <Button title={"回到登录页"} onPress={()=>{
                navigation.navigate('LoginScreen');
            }} />

            <Button title={"详情页面传参测试"} onClick={()=>{
                navigation.navigate('DetailScreen',{
                    screenName: '哈喽，June',
                    url: 'https://www.rmrfjune.cn',
                });
            }}>
                <Text>333</Text>
            </Button>

            <Button title={"leibiao"} onPress={()=>{
                navigation.navigate('MovieList',{
                    screenName: 'list',
                    url: 'https://www.rmrfjune.cn',
                });
            }} />
        </>
    );
};

export default MineScreen

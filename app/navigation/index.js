import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Button from '@app/components/custom-button'
import { Text } from '@app/components/custom-theme'
import { Alert } from 'react-native'
import ScreenOptions from '@app/config/screenOptions'

import TabNav from './tabNav'

import LoginScreen from '@app/views/home'
import DetailScreen from '@app/views/DetailScreen'

import MovieListScreen from '@app/views/MovieListScreen'



const Stack = createStackNavigator()

const App = () => {
    return (
        <Stack.Navigator initialRouteName='TabNav' screenOptions={ ScreenOptions() }>
            <Stack.Screen
                name='LoginScreen'
                component={ LoginScreen }
                options={{ title: '登录', headerShown: false }}
            />
            <Stack.Screen
                name='TabNav'
                component={ TabNav }
                options={{ title: '首页', headerShown: false }}
            />
            <Stack.Screen
                name='DetailScreen'
                component={ DetailScreen }
                options={({ route }) => {
                    return {
                        title: route.params.screenName,
                        headerRight: () => (
                            <Button
                                style={{ padding: px2(10), backgroundColor: '#f34250' }}
                                onClick={ () => {
                                    Alert.alert('anniu')
                                } }
                            >
                                <Text style={{color: '#fff'}}>按钮</Text>
                            </Button>
                        ),
                        ...ScreenOptions()
                    }
                }}
            />
             <Stack.Screen
                name='MovieList'
                component={ MovieListScreen }
                options={{ title: '列表测试', headerShown: true }}
            />
        </Stack.Navigator>
    )
}

export default App
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import AntDesign from 'react-native-vector-icons/AntDesign'
import ScreenOptions from '@app/config/screenOptions'

import HomeScreen from '@app/views/home'
import Test1 from '../views/Test1Screen'
import Test2 from '../views/Test2Screen'
import MineScreen from '@app/views/mine'

const Tab = createBottomTabNavigator()
const TabStack = createStackNavigator()

const Navigation = ({ colorScheme }) => {
    return (
        <Tab.Navigator
            screenOptions={ ({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    if (route.name === 'homeScreen') {
                        return <AntDesign name='home' size={ size } color={ color } />
                    } else if (route.name === 'test1') {
                        return <AntDesign name='cloudo' size={ size } color={ color } />
                    } else if (route.name === 'test2') {
                        return <AntDesign name='mail' size={ size } color={ color } />
                    } else if (route.name === 'mineScreen') {
                        return <AntDesign name='setting' size={ size } color={ color } />
                    }
                }
            }) }
        >
        <Tab.Screen
            name='homeScreen'
            component={ HomeScreen }
            options={{ title: '首页', headerMode: 'none' }}
        />
        <Tab.Screen
          name='test1'
          component={ Test1Navigator }
          options={{ title: '我也' }}
        />
        <Tab.Screen
          name='test2'
          component={ Test2Navigator }
          options={{ title: '不知' }}
        />
  
        <Tab.Screen
          name='mineScreen'
          component={ MineNavigator }
          options={{ title: '我的' }}
        />
      </Tab.Navigator> 
    )
}


const Test1Navigator = () => {
    return (
        <TabStack.Navigator>
            <TabStack.Screen
                name='test1_nav'
                component={ Test1 }
                options={{
                    headerTitle: '测试1',
                    headerLeft: null,
                    headerTitleAlign: 'center'
                }}
            />
        </TabStack.Navigator>
    )
}

const Test2Navigator = () => {
    return (
        <TabStack.Navigator>
            <TabStack.Screen
                name='test2_nav'
                component={ Test2 }
                options={{
                    headerTitle: '测试2在左边',
                    headerLeft: null,
                    headerTitleAlign: 'left'
                }}
            />
        </TabStack.Navigator>
    )
}

const MineNavigator = () => {
    return (
        <TabStack.Navigator screenOptions={ ScreenOptions() }>
            <TabStack.Screen
                name='mineScreen'
                component={ MineScreen }
                options={{
                    headerTitle: '个人中心',
                    headerLeft: null,
                    headerTitleAlign: 'center'
                }}
            />
        </TabStack.Navigator>
    )
}

export default Navigation
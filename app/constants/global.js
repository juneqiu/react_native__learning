import { useTheme } from '@react-navigation/native'
import { Dimensions } from 'react-native'

const window = Dimensions.get('window')

/**
 * 设备大小
 */
global.app_w = window.width
global.app_h = window.height

/**
 * 根据750px的设计稿比例转换成dp
 */
global.px2 = (px) => (px / 750) * window.width
global.px2dp = (px) => (px / 750) * window.width

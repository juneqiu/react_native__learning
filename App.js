/*
 * @Description: Appjs
 * @Author: June
 * @Date: 2020-10-31 18:29:21
 * @LastEditTime: 2020-11-06 13:17:17
 * @LastEditors: June
 */

import React from 'react';
import {
  StatusBar,
  useColorScheme
} from 'react-native';

import {
    DarkTheme,
    DefaultTheme,
    NavigationContainer,
} from '@react-navigation/native';

import Navigation from './app/navigation';

const App = () => {
    const colorScheme = useColorScheme();
    return (
        <NavigationContainer
            theme = { colorScheme === 'dark' ? DarkTheme : DefaultTheme }
        >   
            
            <StatusBar />
            <Navigation />
        </NavigationContainer>
       
    );
}

export default App